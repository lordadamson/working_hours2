const local_storage = window.localStorage;
const status_el = document.getElementById('status');
const hours_el = document.getElementById('hours');
const start_btn = document.getElementById('start_btn');
const pause_btn = document.getElementById('pause_btn');
const clear_btn = document.getElementById('clear_btn');

function die(msg)
{
  alert(msg);
  throw new Error(msg);
}

function start()
{
	if(status() === "Working")
	{
		return;
	}

	local_storage.setItem('status', "Working");

	var t = local_storage.getItem('time');

	if(t === null)
	{
		t = '';
	}

	t += Date.now().toString() + ':';

	local_storage.setItem('time', t);
}

function status()
{
	const iw = local_storage.getItem('status');

	if(iw === null)
	{
		return "Not working";
	}

	return iw;
}

function calculate_hours()
{
	const t = local_storage.getItem('time');

	if(t === null)
	{
		return 0;
	}

	var total_seconds = 0;

	const entries = t.split('\n');

	for(var i = 0; i < entries.length; i++)
	{
		const tokens = entries[i].split(':');

		if(tokens.length < 2)
		{
			break;
		}

		if(tokens[1].length === 0)
		{
			total_seconds += Date.now() - parseInt(tokens[0]);
			break;
		}

		total_seconds += parseInt(tokens[1]) - parseInt(tokens[0])
	}

	return total_seconds / 3600000; // 3600000 to convert from ms to hours
}

function pause()
{
	if(status() === "Not working")
	{
		return;
	}

	local_storage.setItem('status', "Not working");

	var t = local_storage.getItem('time');

	if(t === null)
	{
		die("Couldn't find a time entry while trying to pause");
	}

	t += Date.now().toString() + '\n';

	local_storage.setItem('time', t);
}

function clear()
{
	local_storage.clear();
}

function display()
{
	status_el.innerHTML = status();
	hours_el.innerHTML = calculate_hours().toFixed(2);
}

function _on_start()
{
	start();
	display();
}

function _on_pause()
{
	pause();
	display();
}

function _on_clear()
{
	clear();
	display();
}

function _on_tab_switch()
{
	display();
}

function _on_display_timeout()
{
	display();
	window.setTimeout(_on_display_timeout, 60000);
}

start_btn.addEventListener("click", _on_start);
pause_btn.addEventListener("click", _on_pause);
clear_btn.addEventListener("click", _on_clear);
document.addEventListener("visibilitychange", _on_tab_switch);

display();
window.setTimeout(_on_display_timeout, 60000);
